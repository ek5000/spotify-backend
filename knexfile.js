// Update with your config settings.
require('dotenv').config();

module.exports = {
    client: 'postgres',
    connection: process.env.PG_CONNECTION_STRING,
    pool: {
        min: 2,
        max: 10
    },
    migrations: {
        directory: './src/db/migrations',
        tableName: 'knex_migrations'
    },
    asyncStackTraces: true,
};
