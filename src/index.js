import dotenv from 'dotenv';

import app from './server';
import knex from './server/db/connection';

dotenv.config();

app.listen(process.env.SERVICE_PORT, () => console.log(`Server started at ${'blah'}`));

process.on('unhandledRejection', (error) => {
    // Will print "unhandledRejection err is not defined"
    console.error('unhandledRejection', error.message, error);
});

process.on('exit', () => {
    knex.destory();
});
