import {get} from '../connection';

export async function getMe(access_token, refresh_token) {
    const {response} = await get('/me', access_token, refresh_token);
    return response;
}
