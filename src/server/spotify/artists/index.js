import {getList} from '../connection';

export async function getArtistBatch(access_token, refresh_token, ids) {
    const path = `/artists?ids=${ids.join(',')}`;
    return getList(path, 'artists', access_token, refresh_token);
}
