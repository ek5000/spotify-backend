import {getList} from '../connection';

export async function getAudioFeaturesBatch(access_token, refresh_token, ids) {
    const path = `/audio-features?ids=${ids.join(',')}`;
    return getList(path, 'audio_features', access_token, refresh_token);
}
