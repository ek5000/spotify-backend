import {getPaginated} from '../connection';


const LIBRARY_TRACKS_LIMIT = 50;


export async function* getLibrarySongs(access_token, refresh_token) {
    yield* getPaginated('/me/tracks', access_token, refresh_token, {
        limit: LIBRARY_TRACKS_LIMIT,
    });
}
