import _ from 'lodash';
import async from 'async';

import {getPaginated, post, put} from '../connection';


const PLAYLIST_LIMIT = 50;
const PLAYLIST_TRACK_LIMIT = 100;


export async function* getPlaylists(access_token, refresh_token) {
    yield* getPaginated('/me/playlists', access_token, refresh_token, {
        limit: PLAYLIST_LIMIT,
    });
}

export async function* getPlaylistTracks(access_token, refresh_token, playlistId) {
    yield* getPaginated(`/playlists/${playlistId}/tracks`, access_token, refresh_token, {
        limit: PLAYLIST_TRACK_LIMIT,
    });
}

export async function createPlaylist(access_token, refresh_token, playlistName, userId) {
    return post(
        `/users/${userId}/playlists`,
        {name: playlistName},
        access_token,
        refresh_token,
    );
}

export async function clearPlaylistTracks(access_token, refresh_token, playlistId) {
    return put(
        `/playlists/${playlistId}/tracks`,
        {uris: []},
        access_token,
        refresh_token,
    );
}

export async function addPlaylistTracks(access_token, refresh_token, playlistId, tracks, isOrdered = false) {
    const trackBatches = _.chunk(tracks, PLAYLIST_TRACK_LIMIT);
    const method = isOrdered ? 'eachSeries' : 'each';
    await async[method](
        trackBatches,
        async batch => post(
            `/playlists/${playlistId}/tracks`,
            {uris: batch.map(track => `spotify:track:${track.id}`)},
            access_token,
            refresh_token,
        ),
    );
}

export async function setPlaylistTracks(access_token, refresh_token, playlistId, tracks) {
    await clearPlaylistTracks(access_token, refresh_token, playlistId);
    await addPlaylistTracks(access_token, refresh_token, playlistId, tracks);
}
