import {getList} from '../connection';

export async function getAlbumBatch(access_token, refresh_token, ids) {
    const path = `/albums?ids=${ids.join(',')}`;
    return getList(path, 'albums', access_token, refresh_token);
}
