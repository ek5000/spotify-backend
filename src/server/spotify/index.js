import request from 'request-promise-native';
// require('request-debug')(require('request-promise-native'));
import {PATH as AUTH_PATH} from '../routes/auth';

export async function postAuthCode(code) {
    const {
        SERVICE_SCHEME,
        SERVICE_HOST,
        SERVICE_PORT,
        SPOTIFY_CLIENT_ID,
        SPOTIFY_CLIENT_SECRET,
    } = process.env;
    const uri = `${process.env.SPOTIFY_AUTH_URI}/api/token`;
    const options = {
        form: {
            code,
            grant_type: 'authorization_code',
            redirect_uri: `${SERVICE_SCHEME}://${SERVICE_HOST}:${SERVICE_PORT}/${AUTH_PATH}/spotify-redirect-uri`,
            client_id: SPOTIFY_CLIENT_ID,
            client_secret: SPOTIFY_CLIENT_SECRET,
        },
    };
    const response = await request.post(uri, options);
    return JSON.parse(response);
}
