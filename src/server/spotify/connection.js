import request from 'request-promise-native';
import {URL} from 'url';
import qs from 'qs';
import _ from 'lodash';
import retry from 'async-retry';

import {getRefreshedAuthToken} from './auth';
import {updateSessionByRefreshToken} from '../db/queries/session';


const EXPIRED_TOKEN_ERROR_MESSAGE = 'The access token expired';

function tryParseJson(response, defaultValue = {}) {
    try {
        return JSON.parse(response.body);
    } catch (e) {
        return defaultValue;
    }
}
function needToRefreshToken(response) {
    return _.get(response, 'error.status') === 401 &&
        _.get(response, 'error.message') === EXPIRED_TOKEN_ERROR_MESSAGE;
}

function isAbsoluteUrl(url) {
    try {
        new URL(url); // eslint-disable-line no-new
        return true;
    } catch (e) {
        return false;
    }
}

async function base_request(path, method, body = {}, access_token, refresh_token) {
    const {
        SPOTIFY_API_URI,
    } = process.env;
    const baseUrl = isAbsoluteUrl(path) ? undefined : SPOTIFY_API_URI;
    const stringifiedBody = typeof body === 'object' ? JSON.stringify(body) :
        body.toString ? body.toString() :
            body;
    const response = await request({
        baseUrl,
        uri: path,
        method,
        auth: {
            bearer: access_token,
        },
        resolveWithFullResponse: true,
        simple: false,
        body: method === 'GET' ? undefined : stringifiedBody,
    }).catch((e) => { console.error(e); throw e; });

    const json = await tryParseJson(response);
    if (needToRefreshToken(json)) {
        const {access_token: refresed_access_token} = await getRefreshedAuthToken(refresh_token);
        await updateSessionByRefreshToken(refresh_token, refresed_access_token);
        return base_request(path, method, body, refresed_access_token, refresh_token);
    }

    return {response, json, access_token};
}

async function safeRequest(path, method, body, access_token, refresh_token) {
    return retry(async () => {
        const {response, json, access_token: valid_access_token} = await base_request(path, method, body, access_token, refresh_token);
        if (!(200 < response.statusCode || response.statusCode < 300)) {
            throw new Error(json.error);
        }
        return {response: json, access_token: valid_access_token};
    });
}
export async function get(path, access_token, refresh_token) {
    return safeRequest(path, 'GET', undefined, access_token, refresh_token);
}

export async function post(path, body, access_token, refresh_token) {
    return safeRequest(path, 'POST', body, access_token, refresh_token);
}

export async function put(path, body, access_token, refresh_token) {
    return safeRequest(path, 'PUT', body, access_token, refresh_token);
}

export async function getList(path, listKey, access_token, refresh_token) {
    const {response} = await get(path, access_token, refresh_token);
    return response[listKey];
}

export async function* getPaginated(path, access_token, refresh_token, query = {}) {
    if (path === null) {
        return [];
    }
    const {response, access_token: valid_access_token} = await get(
        `${path}${qs.stringify(query, {addQueryPrefix: true})}`,
        access_token,
        refresh_token,
    );
    const nextPagePromise = getPaginated(response.next, valid_access_token, refresh_token);
    yield* response.items;
    yield* await nextPagePromise;
}
