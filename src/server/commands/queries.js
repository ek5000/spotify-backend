export const happy = {
    op: 'and',
    ops: [
        // {op: 'between', 'attribute': 'valence', 'range': [0.8, 1.0]},
        {op: 'between', 'attribute': 'energy', 'range': [0.8, 1.0]},
        {op: 'between', 'attribute': 'danceability', 'range': [0.8, 1.0]},
        {op: 'between', 'attribute': 'releaseDate', 'range': ['2000-01-01', '2019-01-01']},
        // {op: 'between', 'attribute': 'duration', 'range': ['3 minutes', '3 minutes 30 seconds']},
    ]
};

export const playlist = {
    op: 'in',
    type: 'playlist',
    playlist: '37i9dQZEVXcOkicYZYoPRw',
};
