#!/usr/bin/env node

import { getJoinedView } from '../db/queries';
import {happy} from './queries';
import { getMe } from '../spotify/user';
import { getSession } from '../db/queries/session';
import { createPlaylist, clearPlaylistTracks, setPlaylistTracks } from '../spotify/playlists';
import { queryBuilder } from '../utils';
import knex from '../db/connection';

export default async function createUserPlaylist(session_id, playlistName = 'Happy') {
    const {refresh_token, access_token} = await getSession(session_id);
    const {id: userId} = await getMe(access_token, refresh_token);
    const tracks = await getJoinedView(userId)
        .where(builder => queryBuilder(builder, happy));
    const {response: playlist, access_token: valid_access_token} = await createPlaylist(access_token, refresh_token, playlistName, userId);
    await setPlaylistTracks(valid_access_token, refresh_token, playlist.id, tracks);
}

if (require.main === module) {
    createUserPlaylist(process.argv[2])
        .then(knex.destroy);
}
