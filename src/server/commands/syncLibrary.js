import dotenv from 'dotenv';
import async from 'async';
import AsyncJobIterator from 'async-job-iterator';
import DataLoader from 'dataloader';
import _ from 'lodash';

import {getSession} from '../db/queries/session';
import {getArtistBatch} from '../spotify/artists';
import {getAlbumBatch} from '../spotify/albums';
import {getLibrarySongs} from '../spotify/library';
import {createTrackArtist} from '../db/queries/trackArtists';
import { createOrUpdateArtist } from '../db/queries/artists';
import { createOrUpdateTrack, updateTrack } from '../db/queries/tracks';
import { createOrUpdateAlbum } from '../db/queries/albums';
import { getMe } from '../spotify/user';
import { createOrUpdateUser } from '../db/queries/users';
import { createOrUpdateUserTrack } from '../db/queries/userTracks';
import { getPlaylists, getPlaylistTracks } from '../spotify/playlists';
import { createOrUpdatePlaylistTrack, createOrUpdateUserPlaylist } from '../db/queries/playlists';
import { getAudioFeaturesBatch } from '../spotify/audioFeatures';


dotenv.config();


function formatDate(date) {
    if (date.length === 4) { 
        return `${date}-02-01`;
    }
    if (date.length === 7) {
        return `${date}-01`;
    }
    if (date.length === 10) {
        return date;
    }
    console.error('Unknown date format', date);
    return date;
}


export default async function syncLibrary(session_id) {
    const {refresh_token, access_token} = await getSession(session_id);
    const {id: user_id} = await getMe(access_token, refresh_token);
    await createOrUpdateUser(user_id);

    const iterators = {
        album: new AsyncJobIterator({batching: true, batchSize: 20}),
        artist: new AsyncJobIterator({batching: true, batchSize: 50}),
        audioFeatures: new AsyncJobIterator({batching: true, batchSize: 100}),
    };
    const dataLoaders = {
        album: new DataLoader(keys => getAlbumBatch(access_token, refresh_token, keys)),
        artist: new DataLoader(keys => getArtistBatch(access_token, refresh_token, keys)),
        audioFeatures: new DataLoader(keys => getAudioFeaturesBatch(access_token, refresh_token, keys)),
    };
    await Promise.all([
        fetchArtists(iterators.artist, dataLoaders.artist),
        fetchAlbums(iterators.album, dataLoaders.album),
        fetchAudioFeatures(iterators.audioFeatures, dataLoaders.audioFeatures),
        Promise.all([
            fetchLibrarySongs(access_token, refresh_token, user_id, iterators),
            fetchUserPlaylists(user_id, iterators, access_token, refresh_token),
        ]).then(() => Object.values(iterators).forEach(iterator => iterator.done())),
    ]);
    await createOrUpdateUser(user_id, {synced_at: new Date().toISOString()});
    console.info(`Finished syncing library for session ${session_id} and user ${user_id}`);
}

async function fetchArtists(artistIterator, artistDataLoader) {
    return async.forEach(artistIterator, async (artistIds) => {
        const artists = await artistDataLoader.loadMany(artistIds);
        return Promise.all(artists.map(artist => createOrUpdateArtist(artist.id, {
            name: artist.name,
            genres: artist.genres.join(',').slice(255),
        })));
    });
}

async function fetchAlbums(albumIterator, albumDataLoader) {
    return async.forEach(albumIterator, async (albumIds) => {
        const albums = await albumDataLoader.loadMany(albumIds);
        return Promise.all(albums.map(album => createOrUpdateAlbum(album.id, {
            type: album.album_type,
            release_date: formatDate(album.release_date),
            name: album.name,
            genres: album.genres.join(',').slice(255),
            image: _.get(album, 'images[0].url'),
        })));
    });
}

async function fetchAudioFeatures(audioFeaturesIterator, audioFeaturesDataLoader) { 
    return async.forEach(
        audioFeaturesIterator,
        async (trackIds) => {
            const audioFeatures = await audioFeaturesDataLoader.loadMany(trackIds);
            return Promise.all(audioFeatures.map(audioFeature => updateTrack(
                audioFeature.id,
                {
                    danceability: audioFeature.danceability,
                    tempo: audioFeature.tempo,
                    energy: audioFeature.energy,
                    valence: audioFeature.valence,
                },
            )));
        },
    );
}

async function fetchLibrarySongs(access_token, refresh_token, user_id, iterators) {
    await async.forEach(getLibrarySongs(access_token, refresh_token), async ({ track, added_at }) => {
        await fetchTrackInformation(track, iterators);
        await createOrUpdateUserTrack(track.id, user_id, added_at);
    });

    console.info('Finished syncing songs');
}

async function fetchTrackInformation(track, iterators) {
    if (track.album.id == null || track.album.id === '') { console.log(track); }
    if (track.id == null || track.id === '') { console.log(track); }
    await createOrUpdateAlbum(track.album.id);
    await createOrUpdateTrack(track.id, {
        album_id: track.album.id,
        duration: `${track.duration_ms} milliseconds`,
        name: track.name,
        isrc: _.get(track, 'external_ids.isrc'),
        popularity: track.popularity,
    });
    await async.forEachOf(track.artists, async (artist, index) => {
        await createOrUpdateArtist(artist.id);
        return createTrackArtist(track.id, artist.id, index === 0 ? 'primary' : 'featuring');
    });
    // Push the album, artist, and track along to fetch information later
    iterators.album.push(() => track.album.id);
    iterators.audioFeatures.push(() => track.id);
    track.artists.forEach(artist => iterators.artist.push(() => artist.id));
}

async function fetchUserPlaylists(userId, iterators, access_token, refresh_token) {
    return async.forEach(
        getPlaylists(access_token, refresh_token),
        async playlist => Promise.all([
            await createOrUpdateUserPlaylist(playlist.id, userId, {name: playlist.name}),
            await fetchPlaylistTracks(playlist.id, iterators, access_token, refresh_token),
        ]),
    );
}

async function fetchPlaylistTracks(playlistId, iterators, access_token, refresh_token) {
    return async.forEach(
        getPlaylistTracks(access_token, refresh_token, playlistId),
        async ({track, is_local}) => {
            if (is_local) { // TODO
                return;
            }
            await fetchTrackInformation(track, iterators);
            return createOrUpdatePlaylistTrack(track.id, playlistId);
        },
    );
}
