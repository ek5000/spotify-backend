import express from 'express';
import cookieParser from 'cookie-parser';

import authRouter, {PATH as AUTH_PATH} from '../routes/auth';

const app = express();
app.use(cookieParser());

app.use(`/${AUTH_PATH}`, authRouter);


export default app;
