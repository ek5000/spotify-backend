import { Router } from 'express';
import qs from 'qs';

import { getOrCreateSession, updateSession } from '../../db/queries/session';
import { postAuthCode } from '../../spotify';

const PATH = 'auth';
const SCOPES = [
    'playlist-read-private',
    'playlist-modify-public',
    'playlist-modify-private',
    'playlist-read-collaborative',
    'user-library-read',
    'user-library-modify',
];

const router = new Router();

router.get('/spotify-redirect-uri', async (req, res, next) => {
    const { code, state: session_id } = req.query;
    const response = await postAuthCode(code);
    updateSession(session_id, response.access_token, response.refresh_token);
    res.send('Landing page');
    next();
});

router.get('/login', async (req, res, next) => {
    const {
        SPOTIFY_CLIENT_ID,
        SPOTIFY_AUTH_URI,
        SERVICE_SCHEME,
        SERVICE_HOST,
        SERVICE_PORT,
    } = process.env;
    const session = await getOrCreateSession(req.cookies.session_id);
    const {session_id} = session;
    const queryString = qs.stringify({
        client_id: SPOTIFY_CLIENT_ID,
        response_type: 'code',
        redirect_uri: `${SERVICE_SCHEME}://${SERVICE_HOST}:${SERVICE_PORT}/${PATH}/spotify-redirect-uri`,
        state: session_id,
        scope: SCOPES.join(' '),
    }, { encode: false });

    res.cookie('session_id', session_id);
    res.redirect(`${SPOTIFY_AUTH_URI}/authorize?${queryString}`);
    next();
});

export { PATH };
export default router;