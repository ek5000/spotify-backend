export const ATTRIBUTE_MATCHER = {
    artist: 'SpotifyArtists.id',
    album: 'SpotifyAlbums.id',
    danceability: 'SpotifyTracks.danceability',
    energy: 'SpotifyTracks.energy',
    tempo: 'SpotifyTracks.tempo',
    valence: 'SpotifyTracks.valence',
    releaseDate: 'SpotifyAlbums.release_date',
    duration: 'SpotifyTracks.duration',
    popularity: 'SpotifyTracks.popularity',
};

export default function queryBuilder(builder, query) {
    switch (query.op) {
        case 'and': {
            const comboQuery = builder.where(originalBuilder => queryBuilder(originalBuilder, query.ops[0]));
            query.ops.slice(1).forEach(op => comboQuery.andWhere(originalBuilder => queryBuilder(originalBuilder, op)));
            return comboQuery;
        }
        case 'or': {
            const comboQuery = builder.where(originalBuilder => queryBuilder(originalBuilder, query.ops[0]));
            query.ops.slice(1).forEach(op => comboQuery.orWhere(originalBuilder => queryBuilder(originalBuilder, op)));
            return queryBuilder;
        }
        case 'ne': {
            return builder.whereNot(ATTRIBUTE_MATCHER[query.attribute], query.value);
        }
        case 'eq': {
            return builder.where(ATTRIBUTE_MATCHER[query.attribute], query.value);
        }
        case 'between': {
            return builder.whereBetween(ATTRIBUTE_MATCHER[query.attribute], query.range);
        }
        case 'lt': {
            return builder.where(ATTRIBUTE_MATCHER[query.attribute], '<', query.value);
        }
        case 'gt': {
            return builder.where(ATTRIBUTE_MATCHER[query.attribute], '>', query.value);
        }
        case 'in': {
            switch (query.type) {
                case 'album':
                    return queryBuilder(builder, { op: 'eq', attribute: 'album', value: query.value });
                default:
                    return builder;
            }
        }
        default:
            return builder;
    }
}
