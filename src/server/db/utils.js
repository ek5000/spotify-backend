import knex from './connection';


export function upsert(insertStatement, targetColumn, onConflict = 'NOTHING') {
    return knex.raw(`${insertStatement.toString()} ON CONFLICT ${targetColumn} DO ${onConflict.toString()}`);
}

export function createOrUpdate(tableName, attributes) {
    return upsert(
        knex(tableName).insert(attributes),
        '(id)',
        knex.update(attributes),
    );
}
