import {createOrUpdate} from '../../utils';

export async function createOrUpdateAlbum(id, attributes = {}) {
    return createOrUpdate('SpotifyAlbums', {id, ...attributes});
}
