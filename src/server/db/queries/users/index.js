import {createOrUpdate} from '../../utils';

export async function createOrUpdateUser(id, attributes = {}) {
    return createOrUpdate('SpotifyUsers', {id, ...attributes});
}
