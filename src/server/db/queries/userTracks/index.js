import knex from '../../connection';

import {upsert} from '../../utils';

export async function createOrUpdateUserTrack(trackId, userId, addedAt) {
    return upsert(
        knex('SpotifyUserTracks').insert({track_id: trackId, user_id: userId, added_at: addedAt}),
        'ON CONSTRAINT user_track_unique',
    );
}
