import knex from '../../connection';

import {upsert} from '../../utils';

export async function createOrUpdateUserPlaylist(id, userId, attributes = {}) {
    const columns = {id, user_id: userId, ...attributes};
    return upsert(
        knex('SpotifyUserPlaylists').insert(columns),
        'ON CONSTRAINT user_playlist_unique',
        knex.update(columns),
    );
}

export async function createOrUpdatePlaylistTrack(track_id, playlist_id) {
    return upsert(
        knex('SpotifyPlaylistTracks').insert({track_id, playlist_id}),
        'ON CONSTRAINT playlist_track_unique',
    );
}
