import {createOrUpdate} from '../../utils';

export async function createOrUpdateArtist(id, attributes = {}) {
    return createOrUpdate('SpotifyArtists', {id, ...attributes});
}
