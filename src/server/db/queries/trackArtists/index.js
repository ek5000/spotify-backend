import knex from '../../connection';

import {upsert} from '../../utils';

export async function createTrackArtist(trackId, artistId, type) {
    return await upsert(
        knex('SpotifyTrackArtists').insert({track_id: trackId, artist_id: artistId, type}),
        'ON CONSTRAINT track_artist_unique'
    );
}