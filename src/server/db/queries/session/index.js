import uuid from 'uuid/v4';

import knex from '../../connection';

export async function getOrCreateSession(session_id) {
    const rows = await getOrCreateSessionRows(session_id);
    return rows[0];
}

function getOrCreateSessionRows(session_id) {
    if (!session_id) {
        return createSession(uuid());
    }
    return knex.transaction(
        async trx => knex('SpotifySessions')
            .select('*')
            .where({session_id})
            .transacting(trx)
            .then((rows) => {
                if (rows.length > 0) {
                    return rows;
                }
                return createSession(uuid());
            })
            .then(trx.commit)
            .catch(trx.rollback),
    );
}

async function createSession(session_id) {
    return knex('SpotifySessions').insert({session_id}).returning('*');
}

export async function updateSession(session_id, access_token, refresh_token) {
    return knex('SpotifySessions')
        .where('session_id', session_id)
        .update({access_token, refresh_token});
}

export async function updateSessionByRefreshToken(refresh_token, access_token) {
    return knex('SpotifySessions')
        .where('refresh_token', refresh_token)
        .update({access_token});
}

export async function getSession(session_id) {
    return knex('SpotifySessions')
        .where('session_id', session_id)
        .first();
}
