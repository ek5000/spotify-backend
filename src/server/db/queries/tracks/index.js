import knex from '../../connection';
import {createOrUpdate} from '../../utils';

export async function createOrUpdateTrack(id, attributes = {}) {
    return createOrUpdate('SpotifyTracks', {id, ...attributes});
}

export async function updateTrack(id, attributes = {}) {
    console.log(id, attributes);
    return knex('SpotifyTracks')
        .where('id', id)
        .update(attributes);
}
