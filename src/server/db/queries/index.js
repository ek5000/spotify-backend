import knex from '../connection';

// SELECT * FROM "SpotifyUserTracks"
// LEFT JOIN "SpotifyTracks" ON "SpotifyUserTracks"."track_id" = "SpotifyTracks"."id"
// LEFT JOIN "SpotifyTrackArtists" ON "SpotifyTracks"."id" = "SpotifyTrackArtists"."track_id"
// LEFT JOIN "SpotifyArtists" ON "SpotifyTrackArtists"."artist_id" = "SpotifyArtists"."id";

export function getJoinedView(userId) { 
    return knex('SpotifyUserTracks')
        .column('SpotifyTracks.id', {trackName: 'SpotifyTracks.name', albumName: 'SpotifyAlbums.name'})
        .distinct()
        .where('user_id', userId)
        .leftJoin('SpotifyTracks', 'SpotifyUserTracks.track_id', 'SpotifyTracks.id')
        .leftJoin('SpotifyTrackArtists', 'SpotifyTracks.id', 'SpotifyTrackArtists.track_id')
        .leftJoin('SpotifyArtists', 'SpotifyTrackArtists.artist_id', 'SpotifyArtists.id')
        .leftJoin('SpotifyAlbums', 'SpotifyTracks.album_id', 'SpotifyAlbums.id');
}
