import knex from 'knex';
import config from '../../../knexfile';

const CLIENT = knex(config);

export function getConnection() {
    return knex(config);
}
export default CLIENT;
