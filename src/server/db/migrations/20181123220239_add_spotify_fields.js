
exports.up = async function(knex) {
    await knex.schema.table('SpotifyAlbums', table => {
        table.string('genres');
        table.string('image');
    });

    await knex.schema.table('SpotifyTracks', table => {
        table.renameColumn('external_id', 'isrc');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyAlbums', table => {
        table.dropColumn('genres');
        table.dropColumn('image');
    });
    
    await knex.schema.table('SpotifyTracks', table => {
        table.renameColumn('isrc', 'external_id');
    });
};
