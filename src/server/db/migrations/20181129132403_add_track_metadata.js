
exports.up = async function(knex) {
    await knex.schema.table('SpotifyTracks', table => {
        table.float('danceability');
        table.float('energy');
        table.float('tempo');
        table.float('valence');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyTracks', table => {
        table.dropColumn('danceability');
        table.dropColumn('energy');
        table.dropColumn('tempo');
        table.dropColumn('valence');
    });
};
