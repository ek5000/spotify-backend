
exports.up = async function(knex) {
    await knex.schema.createTable('SpotifySessions', table => {
        table.uuid('session_id');
        table.string('access_token');
        table.string('refresh_token');
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('SpotifySessions');
};
