
exports.up = async function(knex) {
    await knex.schema.createTable('SpotifyPlaylistTracks', table => {
        table.string('track_id').notNullable();
        table.string('playlist_id').notNullable();
        table.foreign('track_id').references('SpotifyTracks.id');
        table.foreign('playlist_id').references('SpotifyUserPlaylists.id');
        table.unique(['track_id', 'playlist_id'], 'playlist_track_unique');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyPlaylistTracks', table => {
        table.dropForeign('track_id');
        table.dropForeign('playlist_id');
        table.dropUnique(['track_id', 'playlist_id'], 'playlist_track_unique');
    });

    await knex.schema.dropTable('SpotifyPlaylistTracks');
};
