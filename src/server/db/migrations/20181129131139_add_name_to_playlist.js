
exports.up = async function(knex) {
    await knex.schema.table('SpotifyUserPlaylists', table => {
        table.string('name');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyUserPlaylists', table => {
        table.dropColumn('name');
    });
};
