
exports.up = async function(knex) {
    await knex.schema.alterTable('SpotifyUserPlaylists', table => {
        table.boolean('isAutoManaged')
            .defaultTo(false)
            .alter();
        table.renameColumn('isAutoManaged', 'is_auto_managed');
    });
};

exports.down = async function(knex) {
    await knex.schema.alterTable('SpotifyUserPlaylists', table => {
        table.boolean('is_auto_managed')
            .defaultTo(null)
            .alter();
        table.renameColumn('is_auto_managed', 'isAutoManaged');
    });
};
