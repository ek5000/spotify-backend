exports.up = async function(knex) {
    await knex.schema.createTable('SpotifyTracks', table => {
        table.string('id').notNullable().primary();
        table.specificType('duration', 'interval');
        table.string('name');
        table.string('external_id');
        table.integer('popularity');
    });

    await knex.schema.createTable('SpotifyAlbums', table => {
        table.string('id').notNullable().primary();
        table.enu('type', ['album', 'compilation', 'single'], {useNative: true, enumName: 'album_type'});
        table.string('name');
        table.date('release_date');
    });

    await knex.schema.createTable('SpotifyArtists', table => {
        table.string('id').notNullable().primary();
        table.string('genres'); // comma separated list, will probably make a separate table later
        table.string('name');
    });

    await knex.schema.createTable('SpotifyTrackArtists', table => {
        table.string('track_id').notNullable();
        table.string('artist_id').notNullable();
        table.foreign('track_id').references('SpotifyTracks.id');
        table.foreign('artist_id').references('SpotifyArtists.id');
    });

    await knex.schema.table('SpotifyTracks', table => {
        table.string('album_id').notNullable();
        table.foreign('album_id').references('SpotifyAlbums.id');
    });
};

exports.down = async function(knex, Promise) {
    await knex.schema.table('SpotifyTracks', table => {
        table.dropForeign('album_id');
    });

    await knex.schema.table('SpotifyTrackArtists', table => {
        table.dropForeign('track_id');
        table.dropForeign('artist_id');
    });

    const tablesWithPrimaryKeys = ['SpotifyArtists', 'SpotifyAlbums', 'SpotifyTracks'];
    await Promise.all(tablesWithPrimaryKeys.map(async tableName => {
        await knex.schema.table(tableName, table => {
            table.dropPrimary();
        });
        return await knex.schema.dropTable(tableName);
    }));

    await knex.schema.dropTable('SpotifyTrackArtists');

    await knex.schema.raw('DROP TYPE album_type');
};
