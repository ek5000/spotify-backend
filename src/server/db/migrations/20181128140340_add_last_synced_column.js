
exports.up = async function(knex) {
    await knex.schema.table('SpotifyUsers', table => {
        table.datetime('synced_at');
    });

    await knex.schema.table('SpotifyUserPlaylists', table => {
        table.datetime('synced_at');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyUsers', table => {
        table.dropColumn('synced_at');
    });
    
    await knex.schema.table('SpotifyUserPlaylists', table => {
        table.dropColumn('synced_at');
    });
};
