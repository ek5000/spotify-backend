
exports.up = async function(knex) {
    await knex.schema.table('SpotifyTrackArtists', table => {
        table.unique(['track_id', 'artist_id'], 'track_artist_unique');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyTrackArtists', table => {
        table.dropUnique(['track_id', 'artist_id'], 'track_artist_unique');
    });
};
