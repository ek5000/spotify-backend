
exports.up = async function(knex) {
    await knex.schema.table('SpotifyTrackArtists', table => {
        table.enu('type', ['primary', 'featuring'], {useNative: true, enumName: 'artist_type'});
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyTrackArtists', table => {
        table.dropColumn('type');
    });

    await knex.schema.raw('DROP TYPE artist_type');
};
