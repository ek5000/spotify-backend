
exports.up = async function(knex) {
    await knex.schema.createTable('SpotifyUsers', table => {
        table.string('id').notNullable().primary();
    });

    await knex.schema.createTable('SpotifyUserPlaylists', table => {
        table.string('id').notNullable().primary();
        table.string('user_id').notNullable();
        table.boolean('isAutoManaged').notNullable();
        table.json('query');
        table.foreign('user_id').references('SpotifyUsers.id');
        table.unique(['id', 'user_id'], 'user_playlist_unique');
    });

    await knex.schema.createTable('SpotifyUserTracks', table => {
        table.string('track_id').notNullable();
        table.string('user_id').notNullable();
        table.datetime('added_at');
        table.foreign('track_id').references('SpotifyTracks.id');
        table.foreign('user_id').references('SpotifyUsers.id');
        table.unique(['track_id', 'user_id'], 'user_track_unique');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('SpotifyUserPlaylists', table => {
        table.dropUnique(['id', 'user_id'], 'user_playlist_unique');
        table.dropForeign('user_id');
        table.dropPrimary();
    });
    await knex.schema.table('SpotifyUserTracks', table => {
        table.dropUnique(['track_id', 'user_id'], 'user_track_unique');
        table.dropForeign('user_id');
    });
    await knex.schema.table('SpotifyUsers', table => {
        table.dropPrimary();
    });
    await knex.schema.dropTable('SpotifyUsers');
    await knex.schema.dropTable('SpotifyUserPlaylists');
    await knex.schema.dropTable('SpotifyUserTracks');
};
